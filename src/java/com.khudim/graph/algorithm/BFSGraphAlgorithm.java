package com.khudim.graph.algorithm;


import com.khudim.graph.core.Edge;
import com.khudim.graph.core.Vertex;

import java.util.*;

public class BFSGraphAlgorithm implements GraphAlgorithm {

    public static final String BFS = "BFS";

    @Override
    public List<Edge> getPath(Vertex start, Vertex finish) {
        var queue = new LinkedList<Vertex>();
        var edgePath = new HashMap<Vertex, Edge>();
        queue.add(start);
        edgePath.put(start, null);

        while (!queue.isEmpty()) {
            var source = queue.poll();
            Set<Edge> edges = source.getLinkEdges();
            for (var edge : edges) {
                var target = edge.getTarget(source);
                if (target == null) {
                    continue;
                }
                if (finish.equals(target)) {
                    return resultPath(finish, edge, edgePath);
                }
                if (!edgePath.containsKey(target)) {
                    edgePath.put(target, edge);
                    queue.push(target);
                }
            }
        }
        return null;
    }

    private List<Edge> resultPath(Vertex finish, Edge edge, Map<Vertex, Edge> edgePath) {
        var prev = edge.getSource(finish);
        var path = new LinkedList<Edge>();
        path.add(edge);

        Edge pathItem;
        while ((pathItem = edgePath.get(prev)) != null) {
            path.addFirst(pathItem);
            prev = pathItem.getSource(prev);
        }

        return path;
    }


}
