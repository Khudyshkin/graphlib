package com.khudim.graph.algorithm;


import com.khudim.graph.core.Edge;
import com.khudim.graph.core.Vertex;

import java.util.*;

public class DFSGraphAlgorithm implements GraphAlgorithm {

    public static final String DFS = "DFS";

    @Override
    public List<Edge> getPath(Vertex start, Vertex finish) {
        Set<Vertex> marked = new HashSet<>();
        Stack<Edge> path = new Stack<>();

        return getPathFromTarget(start, finish, marked, path) ? path : null;
    }

    private boolean getPathFromTarget(Vertex current, Vertex finish, Set<Vertex> marked, Stack<Edge> path) {
        if (finish.equals(current)) {
            return true;
        }
        marked.add(current);

        Set<Edge> edges = current.getLinkEdges();
        for (Edge edge : edges) {
            var target = edge.getTarget(current);
            if (target != null && !marked.contains(target)) {
                path.push(edge);
                if (getPathFromTarget(target, finish, marked, path)) {
                    return true;
                }
            }
        }
        if (!path.isEmpty()) {
            path.pop();
        }
        return false;
    }
}
