package com.khudim.graph.algorithm;


import com.khudim.graph.core.Edge;
import com.khudim.graph.core.Vertex;

import java.util.List;

public interface GraphAlgorithm {
    List<Edge> getPath(Vertex start, Vertex finish);
}
