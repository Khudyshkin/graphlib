package com.khudim.graph.algorithm;

import java.util.Map;

import static com.khudim.graph.algorithm.BFSGraphAlgorithm.BFS;
import static com.khudim.graph.algorithm.DFSGraphAlgorithm.DFS;

public class GraphAlgorithms {

    public static final Map<String, GraphAlgorithm> ALGORITHMS = Map.of(
            DFS, new DFSGraphAlgorithm(),
            BFS, new BFSGraphAlgorithm()
    );

    public static GraphAlgorithm getAlgorithmByName(String algName) {
        if (algName == null) {
            throw new IllegalArgumentException("Algorithm name is empty");
        }
        var algorithm = ALGORITHMS.get(algName);
        if (algorithm == null) {
            throw new IllegalArgumentException("Algorithm not supported");
        }
        return algorithm;
    }
}
