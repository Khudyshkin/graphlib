package com.khudim.graph.core;

import java.util.List;
import java.util.function.Consumer;

public interface IGraph {

    void addVertex(Vertex vertex);

    void addEdge(Vertex from, Vertex to);

    List<Edge> getPath(Vertex start, Vertex finish);

    List<Edge> getPath(Vertex start, Vertex finish, String algName);

    void traverseFunction(Consumer<Vertex> consumer);
}
