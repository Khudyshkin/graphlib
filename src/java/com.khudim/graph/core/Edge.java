package com.khudim.graph.core;

import java.util.Objects;

public abstract class Edge {
    private Vertex beginVertex;
    private Vertex endVertex;
    private Integer weight;

    public Edge(Vertex beginVertex, Vertex endVertex, Integer weight) {
        this.beginVertex = beginVertex;
        this.endVertex = endVertex;
        this.weight = weight;
    }

    public Vertex getBeginVertex() {
        return beginVertex;
    }

    public Vertex getEndVertex() {
        return endVertex;
    }

    public Integer getWeight() {
        return weight;
    }

    public abstract Vertex getTarget(Vertex source);

    public abstract Vertex getSource(Vertex target);

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Edge)) return false;

        Edge edge = (Edge) o;
        return beginVertex.equals(edge.beginVertex) && endVertex.equals(edge.endVertex);

    }

    @Override
    public int hashCode() {
        return Objects.hash(beginVertex, endVertex);
    }
}
