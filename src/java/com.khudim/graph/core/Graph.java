package com.khudim.graph.core;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Consumer;

import static com.khudim.graph.algorithm.BFSGraphAlgorithm.BFS;
import static com.khudim.graph.algorithm.GraphAlgorithms.getAlgorithmByName;


public abstract class Graph implements IGraph {
    private final Set<Edge> edges;
    private final Map<Vertex, Vertex> vertices;

    public Graph() {
        this.edges = ConcurrentHashMap.newKeySet();
        this.vertices = new ConcurrentHashMap<>();
    }

    public void addVertex(Vertex vertex) {
        vertices.putIfAbsent(vertex, vertex);
    }

    public void addEdge(Vertex from, Vertex to) {
        addEdge(from, to, null);
    }

    public void addEdge(Vertex from, Vertex to, Integer weight) {
        addVertex(from);
        addVertex(to);

        Edge edge = createEdge(vertices.get(from), vertices.get(to), weight);

        if (edges.contains(edge)) {
            throw new IllegalArgumentException("Duplicated edge: " + edge);
        } else {
            edges.add(edge);
        }

    }

    public List<Edge> getPath(Vertex start, Vertex finish) {
        return getPath(start, finish, BFS);
    }

    public List<Edge> getPath(Vertex start, Vertex finish, String algName) {
        var alg = getAlgorithmByName(algName);
        var sourceVertex = vertices.get(start);
        if (sourceVertex == null) {
            throw new RuntimeException(("Can't find vertex = " + start));
        }
        var targetVertex = vertices.get(finish);
        if (targetVertex == null) {
            throw new RuntimeException(("Can't find vertex = " + finish));
        }
        return alg.getPath(sourceVertex, targetVertex);
    }

    public void traverseFunction(Consumer<Vertex> consumer) {
        vertices.values().forEach(consumer);
    }

    protected abstract Edge createEdge(Vertex from, Vertex to, Integer weight);

    @Override
    public String toString() {
        return "Graph{" +
                "edges=" + edges +
                ", vertices=" + vertices.values() +
                '}';
    }
}
