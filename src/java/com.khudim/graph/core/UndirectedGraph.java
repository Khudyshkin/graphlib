package com.khudim.graph.core;

public class UndirectedGraph extends Graph {
    @Override
    protected Edge createEdge(Vertex from, Vertex to, Integer weight) {
        return new UndirectedEdge(from, to, weight);
    }
}
