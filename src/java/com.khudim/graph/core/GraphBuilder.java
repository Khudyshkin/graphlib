package com.khudim.graph.core;


public class GraphBuilder {

    private IGraph graph;

    private GraphBuilder(IGraph graph) {
        this.graph = graph;
    }

    public static GraphBuilder newOrientedGraph() {
        return new GraphBuilder(new OrientedGraph());
    }

    public static GraphBuilder newUndirectedGraph() {
        return new GraphBuilder(new UndirectedGraph());
    }

    public static GraphBuilder newDirectedGraph() {
        return new GraphBuilder(new DirectedGraph());
    }

    public GraphBuilder withEdge(String v1, String v2) {
        return withEdge(new Vertex<>(v1), new Vertex<>(v2));
    }

    public GraphBuilder withEdge(Integer v1, Integer v2) {
        return withEdge(new Vertex<>(v1), new Vertex<>(v2));
    }

    public GraphBuilder withEdge(Vertex v1, Vertex v2) {
        graph.addEdge(v1, v2);
        return this;
    }

    public IGraph build() {
        return graph;
    }
}
