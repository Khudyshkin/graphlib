package com.khudim.graph.core;

import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class Vertex<T> {
    private T value;

    private Set<Edge> links = ConcurrentHashMap.newKeySet();

    public Vertex(T value) {
        this.value = value;
    }

    public void addLink(Edge link) {
        links.add(link);
    }

    public T getValue() {
        return value;
    }

    public Set<Edge> getLinkEdges() {
        return links;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex<?> vertex = (Vertex<?>) o;
        return Objects.equals(value, vertex.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return "(" + value + ")";
    }
}
