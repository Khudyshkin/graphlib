package com.khudim.graph.core;

public class DirectedGraph extends Graph {
    @Override
    protected Edge createEdge(Vertex from, Vertex to, Integer weight) {
        return new OrientedEdge(from, to, weight);
    }
}
