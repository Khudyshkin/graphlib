package com.khudim.graph.core;


public class OrientedEdge extends Edge {

    public OrientedEdge(Vertex beginVertex, Vertex endVertex, Integer weight) {
        super(beginVertex, endVertex, weight);
        beginVertex.addLink(this);
    }

    @Override
    public Vertex getTarget(Vertex source) {
        return source.equals(getBeginVertex()) ? getEndVertex() : null;
    }

    @Override
    public Vertex getSource(Vertex target) {
        return target.equals(getEndVertex()) ? getBeginVertex() : null;
    }

    @Override
    public String toString() {
        return getBeginVertex() + " -> " + getEndVertex();
    }
}
