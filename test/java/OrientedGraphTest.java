import com.khudim.graph.core.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static com.khudim.graph.algorithm.BFSGraphAlgorithm.BFS;
import static com.khudim.graph.algorithm.DFSGraphAlgorithm.DFS;


public class OrientedGraphTest {

    @Test
    public void shouldFindPathByDFS() {
        IGraph graph = GraphBuilder.newOrientedGraph()
                .withEdge(1, 3)
                .withEdge(3, 4)
                .withEdge(3, 2)
                .withEdge(4, 5)
                .build();

        Vertex start = new Vertex<>(1);
        Vertex end = new Vertex<>(5);
        List<Edge> path = graph.getPath(start, end, DFS);
        Assert.assertNotNull(path);
        Assert.assertEquals(path.get(0).getBeginVertex(), start);
        Assert.assertEquals(path.get(path.size() - 1).getEndVertex(), end);
    }

    @Test
    public void shouldFindPathByBFS() {
        IGraph graph = GraphBuilder.newOrientedGraph()
                .withEdge(1, 3)
                .withEdge(1, 2)
                .withEdge(3, 4)
                .withEdge(3, 2)
                .withEdge(4, 5)
                .build();
        System.out.println(graph);
        Vertex start = new Vertex<>(1);
        Vertex end = new Vertex<>(5);
        List<Edge> path = graph.getPath(start, end, BFS);
        Assert.assertNotNull(path);
        Assert.assertEquals(path.get(0).getBeginVertex(), start);
        Assert.assertEquals(path.get(path.size() - 1).getEndVertex(), end);
    }

    @Test
    public void shouldNotFindPathByBFS() {
        IGraph graph = GraphBuilder.newOrientedGraph()
                .withEdge(1, 3)
                .withEdge(3, 4)
                .withEdge(3, 2)
                .withEdge(4, 5)
                .build();

        Vertex start = new Vertex<>(2);
        Vertex end = new Vertex<>(5);
        List<Edge> path = graph.getPath(start, end, BFS);
        Assert.assertNull(path);
    }

    @Test
    public void shouldNotFindPathByDFS() {
        IGraph graph = GraphBuilder.newOrientedGraph()
                .withEdge(1, 3)
                .withEdge(3, 4)
                .withEdge(3, 2)
                .withEdge(4, 5)
                .build();

        Vertex start = new Vertex<>(2);
        Vertex end = new Vertex<>(5);
        List<Edge> path = graph.getPath(start, end, DFS);
        Assert.assertNull(path);
    }
}
