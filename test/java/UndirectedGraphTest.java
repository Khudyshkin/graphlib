import com.khudim.graph.core.*;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static com.khudim.graph.algorithm.BFSGraphAlgorithm.BFS;
import static com.khudim.graph.algorithm.DFSGraphAlgorithm.DFS;

public class UndirectedGraphTest {

    @Test
    public void shouldFindPathByDFS() {
        IGraph graph = GraphBuilder.newUndirectedGraph()
                .withEdge(4, 1)
                .withEdge(1, 3)
                .withEdge(3, 5)
                .withEdge(4, 5)
                .withEdge(3, 2)
                .build();
        graph.traverseFunction(System.out::print);
        Vertex start = new Vertex<>(4);
        Vertex end = new Vertex<>(2);
        List<Edge> path = graph.getPath(start, end, DFS);
        Assert.assertNotNull(path);
        Assert.assertEquals(path.get(0).getBeginVertex(), start);
        Assert.assertEquals(path.get(path.size() - 1).getEndVertex(), end);
    }

    @Test
    public void shouldFindPathByBFS() {
        IGraph graph = GraphBuilder.newUndirectedGraph()
                .withEdge(4, 1)
                .withEdge(1, 3)
                .withEdge(3, 5)
                .withEdge(4, 5)
                .withEdge(3, 2)
                .build();

        Vertex start = new Vertex<>(4);
        Vertex end = new Vertex<>(2);
        List<Edge> path = graph.getPath(start, end, BFS);
        Assert.assertNotNull(path);
        Assert.assertEquals(path.get(0).getBeginVertex(), start);
        Assert.assertEquals(path.get(path.size() - 1).getEndVertex(), end);
    }

    @Test
    public void shouldNotFindPathByDFS() {
        IGraph graph = GraphBuilder.newUndirectedGraph()
                .withEdge(4, 1)
                .withEdge(1, 3)
                .withEdge(3, 5)
                .withEdge(4, 5)
                .withEdge(2, 2)
                .build();

        Vertex start = new Vertex<>(4);
        Vertex end = new Vertex<>(2);
        List<Edge> path = graph.getPath(start, end, DFS);
        Assert.assertNull(path);
    }

    @Test
    public void shouldNotFindPathByBFS() {
        IGraph graph = GraphBuilder.newUndirectedGraph()
                .withEdge(4, 1)
                .withEdge(1, 3)
                .withEdge(3, 5)
                .withEdge(4, 5)
                .withEdge(2, 2)
                .build();

        Vertex start = new Vertex<>(4);
        Vertex end = new Vertex<>(2);
        List<Edge> path = graph.getPath(start, end);
        Assert.assertNull(path);
    }
}
