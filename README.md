# Simple implementation of graph library
## Supported algorithms:
        BFS - Breadth-first search
        DFS - Depth-first search
        
## Supported graphs: 
        UndirectedGraph    
        OrientedGraph     
        
## Example:
        IGraph graph = GraphBuilder.newUndirectedGraph()
                      .withEdge(4, 1)
                      .withEdge(1, 3)
                      .withEdge(3, 5)
                      .withEdge(4, 5)
                      .withEdge(3, 2)
                      .build();
        
        Vertex start = new Vertex<>(4);
        Vertex end = new Vertex<>(2);
        List<Edge> path = graph.getPath(start, end, BFS);        